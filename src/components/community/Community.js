import React, {Component} from 'react';
import './Community.css'
import { Redirect } from 'react-router';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Dropdown from 'react-bootstrap/Dropdown';

class Community extends Component {
    state = {
        isreturnHome: false
    }
    returnHome = ()=>{
        console.log("return home");
        this.setState({isreturnHome: true});
    }
    render(){
        let redirect = null;
        if(this.state.isreturnHome){
            redirect = <Redirect to="/posts"/>;
         }

        return (
            <div>
                Hellow Community!
                <br />
                <button onClick={()=> this.returnHome()}>
                    Return to Home!
                </button>
                <Button>React Bootstrap btn</Button>
                <Card style={{ width: '18rem' }}>
                  <Card.Img variant="top" src="https://www.w3schools.com/howto/img_avatar.png" />
                  <Card.Body>
                      <Card.Title>Card Title</Card.Title>
                      <Card.Text>
                      Some quick example text to build on the card title and make up the bulk of
                      the card's content.
                      </Card.Text>
                      <Button variant="primary">Go somewhere</Button>
                  </Card.Body>
                </Card>
                <Dropdown>
                    <Dropdown.Toggle variant="success" id="dropdown-basic">
                        Dropdown Button
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
                        <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
                <br />
                {redirect}
            </div>
        );
    }
}

export default Community;