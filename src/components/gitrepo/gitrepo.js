import React from 'react';

const gitrepo = (props) => (
    <tr>
        <td>{props.id}</td>
        <td>{props.name}</td>
        <td>{props.git_url}</td>
    </tr>
);

export default gitrepo;