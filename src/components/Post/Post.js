import React from 'react';
import './Post.css';

const post = (props) => (
    <article className="Post">
        <h1>{props.title}</h1>
        <p>id: {props.id}</p>
    </article>
);

export default post;