import React, {Component} from 'react';
import './Layout.css';
import {Navbar, Nav, Form, FormControl, Button} from 'react-bootstrap';

class Layout extends Component {
    render(){
        return (
            <div className="Layout">
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand href="#home">Navbar</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href="/posts">Posts</Nav.Link>
                        <Nav.Link href="/community">Community</Nav.Link>
                        <Nav.Link href="/Repo">Repo</Nav.Link>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-light">Search</Button>
                    </Form>
                </Navbar>

                {this.props.children}
            </div>
        );
    }
}

export default Layout;