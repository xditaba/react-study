import React, {Component} from 'react';
import Layout from './components/header/Layout';
import { BrowserRouter } from 'react-router-dom';
import { Switch, Route } from 'react-router';
import Posts from './containers/Posts/Posts';
import Community from './components/community/Community';
import Gitrepos from './containers/Gitrepos/gitrepos';

class App extends Component {
  render() {
    let routes = (
      <Switch>
        <Route path="/posts" component={Posts} />
        <Route path="/community" component={Community} />
        <Route path="/repo" component={Gitrepos} />
        <Route render={()=><h1>Not Found</h1>}/>
      </Switch>
    );
    return (
      <BrowserRouter>
        <div>
            <Layout >
              {routes}
            </Layout>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
