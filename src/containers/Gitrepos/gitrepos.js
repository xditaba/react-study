import React, {Component} from 'react';
import {Table} from 'react-bootstrap';
import Gitrepo from '../../components/gitrepo/gitrepo';

class Gitrepos extends Component {
  constructor(props){
      super(props);
      this.state = {
        git: []
      };
  }
  componentWillMount(){
    let URL='https://api.github.com/users/xditaba/repos';
    fetch(URL).then((res)=>res.json())
    .then((res)=>{
      this.setState({git: res});
      console.log(res);
    });

  }
  render() {
      let bodytb = (            
        <tr>
          <td>-</td>
          <td>-</td>
          <td>-</td>
        </tr>);
      if(this.state.git && this.state.git.length){
        bodytb = this.state.git.map((git, key) => {
          return (
            <Gitrepo key={key} id={key} name={git.name} git_url={git.git_url} />
          );
        });
      }
      return (
        <div>
          <Table responsive>
            <thead>
              <tr>
                <th>#</th>
                <th>Git Name</th>
                <th>Git Url</th>
              </tr>
            </thead>
            <tbody>
              {bodytb}
            </tbody>
          </Table>
        </div>
      );
  }
}

export default Gitrepos;
