import React, {Component} from 'react';
import './Posts.css';
import Post from '../../components/Post/Post'

class Posts extends Component {
    constructor(props){
        super(props);
        this.state = { posts: [] };
    }
    componentWillMount(){
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(res => {return (res.json())})
            .then(res => {
                res = res.slice(0,4);
                this.setState({posts: res});
        });
    }
    componentWillUpdate(){
    }
    render(){
        var post =  <p>Smt wrong!</p>
        if(this.state.posts && this.state.posts.length){
            post= this.state.posts.map((d) => {
                return (<Post id={d.id} title={d.title} />);
            });
        }
        return (
            <div>
                <section className="Posts">
                    {post}
                </section>
            </div>
        );
    }
}

export default Posts;